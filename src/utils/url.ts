import { map } from "lodash";

export function encodeUri(params: object): string {
  const searchParams = map(
    params,
    (param, key) => `${encodeURIComponent(key)}=${encodeURIComponent(param)}`
  ).join("&");
  return `?${searchParams}`;
}

export function decodeUri(uri: string) {
  const urlSearch = new URLSearchParams(uri);
  const urlSearchParams: { [key: string]: string } = {};
  // eslint-disable-next-line
  for (const param of urlSearch.entries()) {
    urlSearchParams[param[0]] = param[1];
  }
  return urlSearchParams;
}

export function UrlExtractor(url: string) {
  const splitter = url.split("/").slice(1);
  let reverse: any = [];
  for (let i = splitter.length - 1; i >= 0; i = i - 2) {
    const el = [splitter[i - 1], splitter[i]];
    reverse.push(el);
  }
  return reverse;
}
