import { isNaN } from 'lodash';

export function isNumberEmpty(value: any) {
  if (value !== '' && !isNaN(value - 0)) return false;
  return true;
}

export function isStringEmpty(value: string) {
  if (value) return '';
  return 'Champ vide';
}
