/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  useEffect,
  useRef,
  EffectCallback,
  useState,
  useLayoutEffect,
} from "react";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import type { RootState, AppDispatch } from "../configs/store";

// redux state type for select and dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

// did mount effect
export function useDidMount(fn: EffectCallback) {
  const mounted = useRef(false);
  useEffect(() => {
    mounted.current = true;
    fn();
  }, []);
  return mounted.current;
}
// did update effect
export function useDidUpdate(fn: EffectCallback, deps?: any[]) {
  const mounted = useRef(false);
  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true;
    } else {
      fn();
    }
  }, deps);
  return mounted.current;
}
// will unmount effect
export function useWillUnmount(fn: () => void) {
  useEffect(() => () => fn(), []);
}

// input handler hook
export function useTextInput<T>(
  initialValue: T
): [
  T,
  React.Dispatch<React.SetStateAction<T>>,
  (e: React.ChangeEvent<HTMLInputElement>) => void,
  boolean
] {
  const [value, setValue] = useState<T>(initialValue);
  const touched = useRef(false);
  function onChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
    if (!touched.current) {
      touched.current = true;
    }
    if (e.target.type === "checkbox" || e.target.type === "radio") {
      setValue(e.target.checked as any);
    } else if (e.target.type === "file" && e.target.files) {
      setValue(e.target.files as any);
    } else if (e.target.type === "text") {
      setValue(e.target.value as any);
    }
  }
  return [value, setValue, onChange, touched.current];
}

// focus on input hook

export function useFocus() {
  const htmlElRef: any = useRef(null);
  const setFocus = () => {
    htmlElRef.current && htmlElRef.current.focus();
  };

  return [htmlElRef, setFocus];
}

export function useFocusInput(
  array: React.RefObject<HTMLInputElement>[],
  selectedInput: string | undefined
) {
  useLayoutEffect(() => {
    const selectedRef = array.find((el) => el.current?.name === selectedInput);
    if (selectedRef) {
      selectedRef?.current?.focus();
    }
  }, [selectedInput]);
}


