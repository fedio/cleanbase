import TabExemple from "components/tabexemple";
import { useAppSelector, useDidMount } from "utils/hooks";
import { patchParentNav } from "reducers/parents";
import TabNavigation from "components/tabNavigation/tabNavigation";
import navContext from "context/context";
import { useNavigate } from "react-router";

const HomeContainer = () => {
  const navValue = useAppSelector((state) => state.parent.present.nav);
  const navAction = patchParentNav;
  const navigate = useNavigate();

  useDidMount(() => {
    navigate("tab/tab1");
  });

  return (
    <div>
      <navContext.Provider value={{ navValue, navAction }}>
        <TabNavigation
          navValue={navValue}
          navAction={navAction}
          tabKey="tab"
          isParent
        >
          <TabExemple name="tab1" hasChilds={true} tabKey="subTab">
            <TabExemple name="tab1-1" hasChilds={true} tabKey="childTab">
              <TabExemple name="tab1-1-1" hasChilds={true} tabKey="level3Tab" />
              <TabExemple name="tab1-1-2" hasChilds={true} tabKey="level3Tab" />
            </TabExemple>
            <TabExemple name="tab1-2" hasChilds={true} tabKey="childTab">
              <TabExemple name="tab1-2-1" hasChilds={true} tabKey="level3Tab" />
              <TabExemple name="tab1-2-2" hasChilds={true} tabKey="level3Tab" />
            </TabExemple>
          </TabExemple>
          <TabExemple name="tab2" hasChilds={true} tabKey="subTab">
            <TabExemple name="tab2-1" hasChilds={true} tabKey="childTab">
              <TabExemple name="tab2-1-1" hasChilds={true} tabKey="level3Tab" />
              <TabExemple name="tab2-1-2" hasChilds={true} tabKey="level3Tab" />
            </TabExemple>
            <TabExemple name="tab2-2" hasChilds={true} tabKey="childTab">
              <TabExemple name="tab2-2-1" hasChilds={true} tabKey="level3Tab" />
              <TabExemple name="tab2-2-2" hasChilds={true} tabKey="level3Tab" />
            </TabExemple>
          </TabExemple>
        </TabNavigation>
      </navContext.Provider>
    </div>
  );
};

export default HomeContainer;
