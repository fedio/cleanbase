import TopBar from "components/topbar/topBar";
import { Route, Routes } from "react-router-dom";
import { allowDispatch, blockDispatch } from "reducers/dispatchAllow";
import { ActionCreators } from "redux-undo";
import { useAppDispatch, useAppSelector } from "utils/hooks";
import HomeContainer from "./homeContainer";

const RootContainer = () => {
  const dispatch = useAppDispatch();
  const appState = useAppSelector((state) => state.parent);

  const handleUndo = () => {
    dispatch(allowDispatch());
    dispatch(ActionCreators.undo());
  };
  const handleRedo = () => {
    dispatch(blockDispatch());
    dispatch(ActionCreators.redo());
  };

  return (
    <div>
      <TopBar
        close={() => dispatch(ActionCreators.clearHistory())}
        undo={handleUndo}
        redo={handleRedo}
        title={""}
        undoableState={appState}
      />

      <Routes>
        <Route path="/*" element={<HomeContainer />} />
      </Routes>
    </div>
  );
};

export default RootContainer;
