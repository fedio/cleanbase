import "./App.css";
import { MemoryRouter } from "react-router";
import RootContainer from "containers/rootContainer";
import { Provider } from "react-redux";
import store from "configs/store";

function App() {
  return (
    <Provider store={store}>
      <MemoryRouter>
        <RootContainer />
      </MemoryRouter>
    </Provider>
  );
}

export default App;
