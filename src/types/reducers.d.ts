export interface typeNavigation {
  parent: string;
  tab: string;
  subParent: string;
  subTab: string;
  childParent: string;
  childTab: string;
  level3Parent: string;
  level3Tab: string;
  selectedInput: string;
  canDispatch?: boolean;
}
export interface actionData {
  actionLabel: string;
}

export type University = {
  id: string;
  university: string;
  year: number;
};

export type Children = {
  id: string;
  ssn: string;
  first_name: string;
  last_name: string;
  gender: string;
  country: string | null;
  university: string | null;
  previous_universities?: University;
};

export type CreditCard = {
  id: string;
  number: string;
  type: string;
  expiry: string;
  country: string;
};

export type Parent = {
  id: string;
  ssn: string;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  birth_country: string;
  children: Children[];
  credit_cards: CreditCard[];
};

export type reducerType = {
  parents: Parent[];
  nav: typeNavigation;
  actionData: actionData[];
};

export type navParams = {
  parentId: string;
  tabId: string;
  subTab?: string;
  subList?: string;
};

export interface NavContext {
  path: string;
}
