import { State } from "history";
import React, {
  useState,
  ReactNode,
  ReactNodeArray,
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
} from "react";

const LIMIT = 100;
function isCtrl(event: KeyboardEvent): boolean {
  if (window.navigator.userAgent.indexOf("Mac OS X") >= 0) {
    return event.metaKey;
  }

  return event.ctrlKey;
}

type SetState<T> = Dispatch<SetStateAction<T>>;
interface ContextType<T> {
  content: T;
  setContent: SetState<T>;
  isUndoable: boolean;
  isRedoable: boolean;
  undo(): void;
  redo(): void;
}

const UndoableContext = createContext<ContextType<any> | null>(null);

interface ProviderProps {
  children: ReactNode | ReactNodeArray;
}

export function UndoableProvider({children}: ProviderProps) {
  const [past, setPast] = useState<Array<State>>([]);
  const [present, setPresent] = useState<State>({});
  const [future, setFuture] = useState<Array<State>>([]);

  function setContent(action: SetStateAction<State[]>) {
    setPast((prev) => [present, ...prev].slice(0, LIMIT));
    //console.log(typeof action);
    if (typeof action === typeof present) {
      setPresent(action);
    } else {
      setPresent((prev) => prev);
    }
  }

  const undo = () => {
    if (past.length < 1 || past.length > LIMIT) return;
    const [last, ...rest] = past;
    setPast(rest);
    setFuture((prev) => [present, ...prev]);
    setPresent(last);
  };

  const redo = () => {
    if (future.length < 1 || future.length > LIMIT) return;
    const [first, ...rest] = future;
    setFuture(rest);
    setPresent(first);
    setPast((prev) => [present, ...prev]);
  };

  return (
    <UndoableContext.Provider
      value={{
        content: present,
        setContent,
        undo,
        redo,
        isUndoable: past.length > 0,
        isRedoable: future.length > 0,
      }}
    >
      {children}
    </UndoableContext.Provider>
  );
}

export function useUndoable() {
  const state = useContext(UndoableContext);
  if (state == null) {
    throw new Error("context must not be null!");
  }
  return state;
}

export function UndoButton() {
  const { undo, isUndoable } = useUndoable();

  return (
    <button disabled={!isUndoable} onClick={undo}>
      Undo
    </button>
  );
}

export function RedoButton() {
  const { redo, isRedoable } = useUndoable();

  return (
    <button disabled={!isRedoable} onClick={redo}>
      Redo
    </button>
  );
}

export function UndoableKeyboardManager() {
  const { undo, redo } = useUndoable();

  React.useEffect(() => {
    const handleCtrl = (event: KeyboardEvent) => {
      const { code } = event;

      const ctrl = isCtrl(event);

      if (!ctrl) {
        return;
      }

      switch (code) {
        case "y":
          redo();
          break;
        case "z":
          undo();
          break;
        default:
      }
    };

    document.addEventListener("keydown", handleCtrl);

    return () => document.removeEventListener("keydown", handleCtrl);
  }, [undo, redo]);

  return null;
}
