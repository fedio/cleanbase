import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { createContext } from "react";

const navContext = createContext<{
  navValue: any;
  navAction: ActionCreatorWithPayload<any> | null;
}>({
  navValue: {},
  navAction: null,
});

export default navContext;
