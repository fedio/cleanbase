import { combineReducers } from "@reduxjs/toolkit";
import undoable from "redux-undo";

import parents from "./parents";
import allowDispatch from "./dispatchAllow";
import lastAction from "./lastAction";

export const rootReducer = combineReducers({
  parent: undoable(parents, { neverSkipReducer: false }),
  allowDispatch: allowDispatch,
  lastAction: lastAction,
});

export default rootReducer;
