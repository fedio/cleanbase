import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "configs/store";
import {
  actionData,
  Children,
  Parent,
  reducerType,
  typeNavigation,
} from "types/reducers";

const INITIAL_STATE: reducerType = {
  parents: [],
  nav: {
    parent: "",
    tab: "",
    subParent: "",
    subTab: "",
    childParent: "",
    childTab: "",
    level3Parent: "",
    level3Tab: "",
    selectedInput: "",
    canDispatch: true,
  },
  actionData: [],
};

const parentsSlice = createSlice({
  name: "parent",
  initialState: INITIAL_STATE,
  reducers: {
    createParent: (
      state: reducerType,
      action: PayloadAction<Parent & typeNavigation & actionData>
    ) => {
      const {
        ssn,
        actionLabel,
        tab: tabPath,
        parent: ParentPath,
        birth_country,
        last_name,
        gender,
        first_name,
        children,
        credit_cards,
        email,
        id,
      } = action.payload;
      const parentObject: Parent = {
        id,
        ssn,
        first_name,
        last_name,
        email,
        gender,
        birth_country,
        children,
        credit_cards,
      };
      if (parentObject) state.parents.push(parentObject);
      state.nav = {
        ...state.nav,
        parent: ParentPath,
        tab: tabPath,
      };
      state.actionData.push({ actionLabel: actionLabel });
      // do what ever u want to create ur data
    },
    deleteParent: (
      state: reducerType,
      action: PayloadAction<{ id: string } & actionData>
    ) => {
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      const index = state.parents.findIndex(
        (el) => el.id === action.payload.id
      );

      if (index > 0) {
        state.nav.parent = state.parents[index - 1].id;
      }
      state.parents.splice(index, 1);
      if (state.parents.length === 0) {
        state.nav.parent = "";
        state.nav.tab = "";
      }
    },
    patchParent: (
      state: reducerType,
      action: PayloadAction<Parent & actionData>
    ) => {
      // patch ur data how ever u see the right way

      state.actionData.push({ actionLabel: action.payload.actionLabel });
      const parentIndex = state.parents.findIndex(
        (el) => el?.ssn === action.payload.ssn
      );
      state.parents.splice(parentIndex, 1, action.payload);
    },

    patchParentNav: (
      state: reducerType,
      action: PayloadAction<typeNavigation & actionData>
    ) => {
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      state.nav = { ...state.nav, ...action.payload };
    },

    createChildren: (
      state: reducerType,
      action: PayloadAction<Children & typeNavigation & actionData>
    ) => {
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      const childrenObject: Children = {
        id: action.payload.id,
        ssn: action.payload.ssn,
        first_name: action.payload.first_name,
        last_name: action.payload.last_name,
        country: action.payload.country,
        gender: action.payload.gender,
        university: action.payload.university,
      };
      state.parents
        .find((el) => el.id === action.payload.parent)
        ?.children.push(childrenObject);
    },
    patchChildren: (
      state: reducerType,
      action: PayloadAction<Parent & typeNavigation & actionData>
    ) => {
      // patch ur data how ever u see the right way
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      const parentIndex = state.parents
        .find((el) => el?.id === action.payload.parent)
        ?.children.findIndex((item) => item.id === action.payload.subParent);
      parentIndex && state.parents.splice(parentIndex, 1, action.payload);
      const { parent, tab, subParent, subTab } = action.payload;
      state.nav = {
        ...state.nav,
        parent,
        tab,
        subTab,
        subParent,
      };
    },
    deleteChildren: (
      state: reducerType,
      action: PayloadAction<{ childId: string; parent: string } & actionData>
    ) => {
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      const index = state.parents
        .find((el) => el.id === action.payload.parent)
        ?.children.findIndex((item) => item.id === action.payload.childId);

      /* if (index && index > 0) {
        state.nav.subParent = state.parents.find(
          (el) => el.id === action.payload.parent
        )?.children[index - 1].id;
      } */
      index && state.parents.splice(index, 1);
      if (state.parents.length === 0) {
        state.nav.subParent = "";
      }
    },
    patchSubNav: (
      state: reducerType,
      action: PayloadAction<typeNavigation & actionData>
    ) => {
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      state.nav = { ...state.nav, subTab: action.payload.subTab };
    },
    patchChildNav: (
      state: reducerType,
      action: PayloadAction<typeNavigation & actionData>
    ) => {
      state.actionData.push({ actionLabel: action.payload.actionLabel });
      state.nav = { ...state.nav, childTab: action.payload.childTab };
    },
    canDispatchAction: (state: reducerType) => {
      state.nav.canDispatch = true;
    },
    canDispatchtoggle: (state: reducerType) => {
      state.nav.canDispatch = false;
    },
  },
});

export const {
  createParent,
  deleteParent,
  patchParent,
  patchParentNav,
  createChildren,
  patchChildren,
  deleteChildren,
  patchSubNav,
  patchChildNav,
  canDispatchAction,
  canDispatchtoggle,
} = parentsSlice.actions;

export const selectParent = (state: RootState) => state.parent;

export default parentsSlice.reducer;
