import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "configs/store";

interface CanDispatch {
  allowDispatch: boolean;
}

const INITIAL_STATE: CanDispatch = {
  allowDispatch: true,
};

const allowSlice = createSlice({
  name: "allow",
  initialState: INITIAL_STATE,
  reducers: {
    allowDispatch: (state: CanDispatch) => {
      state.allowDispatch = true;
    },
    blockDispatch: (state: CanDispatch) => {
      state.allowDispatch = false;
    },
  },
});

export const { allowDispatch, blockDispatch } = allowSlice.actions;

export const selectAllower = (state: RootState) => state.allowDispatch;

export default allowSlice.reducer;
