/* eslint-disable react-hooks/exhaustive-deps */
import React, { Children, useCallback, useEffect, useState } from "react";
import classes from "./style.module.scss";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import TabPanel from "components/tabPanel/tabPanel";
import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { useAppDispatch, useAppSelector, useDidMount } from "utils/hooks";
import { Route, Routes, useNavigate, useLocation } from "react-router";

interface Props {
  children: React.ReactElement | React.ReactElement[];
  navValue: any;
  navAction: ActionCreatorWithPayload<any> | null;
  tabKey: string;
  isParent?: boolean;
}

const TabNavigation: React.FC<Props> = ({
  children,
  navValue,
  navAction,
  tabKey,
  isParent,
}: Props) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const allower = useAppSelector((state) => state.allowDispatch.allowDispatch);
  const location = useLocation();

  //console.log(location.pathname);
  // states-------------------------------------------------
  const [tabIndex, setTabIndex] = useState(0);
  const data = Children.map(children, (child) => child.props);
  const [tabName, setTabName] = useState("");
  // handlers-----------------------------------------------
  const tabsChange = (e: React.SyntheticEvent, newValue: number) => {
    setTabIndex(newValue);
    const name = e.currentTarget.textContent;
    if (name) setTabName(name);
    if (navAction) {
      /* if (data.length > 0) {
        const withChildrenName = data.find((el) => el.children?.length > 0)
          ?.children[0]?.props.name;
        const withChildrenTab = data.find(
          (el) => el.children?.length > 0
        )?.tabKey;
        //---------------------------------------------------------------------
        navigate(`${tabKey}/${name}`);
        dispatch(
          navAction({
            [tabKey]: name,
            [withChildrenTab]: withChildrenName,
            actionLabel: `naviger vers ${name}`,
          })
        );
      } else { */
      navigate(`${tabKey}/${name}`);
      dispatch(
        navAction({
          [tabKey]: name,
          actionLabel: `naviger vers ${name}`,
        })
      );
      /* } */
    }
  };

  const indexFinder = useCallback(() => {
    if (data.indexOf(data.filter((el) => el.name === navValue[tabKey])) !== -1) {
      setTabIndex(
        data.indexOf(data.find((el) => el.name === navValue[tabKey]))
      );
    }
  }, [navValue]);

  // effects------------------------------------------------
  useEffect(() => {
    indexFinder();
  }, [tabName, indexFinder]);
  useDidMount(() => {
    if (data.length > 0) {
      navigate(`${tabKey}/${data[0].name}`);
    }

    if (data.map((el) => el.children?.length === 0) && isParent) {
      if (navAction) {
        if (allower) {
          dispatch(
            navAction({
              [tabKey]: data[0].name,
              actionLabel: `naviger vers ${data[0].name}`,
            })
          );
        }
      }
    }
  });

  //jsx-----------------------------------------------------
  return (
    <div className={classes.container}>
      <Tabs
        value={tabIndex}
        onChange={tabsChange}
        variant="scrollable"
        scrollButtons="auto"
        autoFocus={true}
      >
        {data.map((el: any, index: number) => {
          return <Tab label={el && el.name} key={index} />;
        })}
      </Tabs>
      {Children.map(children, (child, index) => {
        return (
          <Routes>
            <Route
              path={`${tabKey}/:${tabKey}Id/*`}
              element={
                <TabPanel key={index} index={index} value={tabIndex}>
                  {child}
                </TabPanel>
              }
            />
          </Routes>
        );
      })}
    </div>
  );
};
export default TabNavigation;

TabNavigation.defaultProps = {
  isParent: false,
};
