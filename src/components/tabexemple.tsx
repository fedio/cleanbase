import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import navContext from "context/context";
import { useContext } from "react";
import TabNavigation from "./tabNavigation/tabNavigation";

interface Props {
  name: string;
  hasChilds: boolean;
  children?: React.ReactElement | React.ReactElement[];
  action?: ActionCreatorWithPayload<any>;
  tabKey?: string;
}

const TabExemple = ({ name, hasChilds, children, tabKey }: Props) => {
  const { navValue, navAction } = useContext(navContext);

  return (
    <div>
      this is a tab with a name {name}
      {hasChilds && children && navAction && tabKey && (
        <div>
          <TabNavigation
            navValue={navValue}
            navAction={navAction}
            tabKey={tabKey}
          >
            {children}
          </TabNavigation>
        </div>
      )}
    </div>
  );
};

export default TabExemple;
