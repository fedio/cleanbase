import React from "react";
import { Route } from "react-router-dom";
import TopBar from "../topbar/topBar";

interface Props {
  path: string;
  element: React.ReactElement | null;
}

const CustomRoute = ({ path, element }: Props) => {
  return (
    <div>
      <Route path={path} element={element} />
    </div>
  );
};

export default CustomRoute;
