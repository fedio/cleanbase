import React, { useEffect, useState } from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { VscChevronDown } from "react-icons/vsc";
import { ActionCreators, StateWithHistory } from "redux-undo";
import { useAppDispatch } from "utils/hooks";
import { actionData } from "types/reducers";

interface Props {
  direction: "forward" | "backward";
  data: StateWithHistory<any>;
}

export default function BasicMenu({ direction, data }: Props) {
  const dispatch = useAppDispatch();
  const [useable, setUseable] = useState<any>([]);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<any>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAction = (index: number) => {
    if (direction === "backward") {
      dispatch(ActionCreators.jumpToPast(index));
    } else {
      dispatch(ActionCreators.jumpToFuture(index));
    }
    setAnchorEl(null);
  };

  useEffect(() => {
    if (direction === "forward") {
      if (data.index) {
        const reversed = data.future[data.future.length - 1]?.actionData.filter(
          (el: actionData, index: number) => data?.index && data.index <= index
        );
        setUseable(reversed);
      }
    } else {
      setUseable(data?.present?.actionData);
    }
  }, [direction, data.present.actionData, data.future, data.index]);

  return (
    <div>
      <VscChevronDown
        id="basic-button"
        aria-controls="basic-menu"
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      />
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        {useable?.map((el: any, index: number) => {
          return (
            <MenuItem onClick={() => handleAction(index)} key={index}>
              {el.actionLabel}
            </MenuItem>
          );
        })}
      </Menu>
    </div>
  );
}
