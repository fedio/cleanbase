import classes from "./style.module.scss";
import { VscChromeClose, VscArrowRight, VscArrowLeft } from "react-icons/vsc";
import DropDown from "components/dropdown/dropdown";
import { StateWithHistory } from "redux-undo";
import Button from "@mui/material/Button";

// types--------------------------------------
interface Props {
  close: () => void;
  undo: () => void;
  redo: () => void;
  title: string;
  undoableState: StateWithHistory<any>;
}

const TopBar = ({ close, undo, redo, title, undoableState }: Props) => {
  return (
    <div className={classes.container}>
      <div className={classes.left_part}>
        <Button color="error" variant="outlined">
          <VscChromeClose onClick={close} className={classes.iconStyle} />
        </Button>
        <Button color="info" variant="text" onClick={undo}>
          <VscArrowLeft color={"black"} className={classes.iconStyle} />
        </Button>
        <DropDown direction="backward" data={undoableState} />

        <Button color="info" variant="text" onClick={redo}>
          <VscArrowRight color={"black"} className={classes.iconStyle} />
        </Button>
        <DropDown direction="forward" data={undoableState} />
      </div>
      <div className={classes.center_part}>
        <span>{title}</span>
      </div>
      <div className={classes.right_part}>
        <Button variant="outlined" color="secondary">
          save
        </Button>
      </div>
    </div>
  );
};
export default TopBar;
