import { configureStore } from "@reduxjs/toolkit";
import { Middleware } from "redux";
import { createLogger } from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import rootReducer from "reducers";

// middlewares --------------------------------------
const middlewares: Middleware[] = [];

if (process.env.NODE_ENV === "development") {
  const logger = createLogger();
  middlewares.push(logger);
}
// persist reducer-----------------------------------

const persistConfig = {
  key: "root",
  version: 1,
  storage,
  whitelist: [],
  blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
// store --------------------------------------------
export const store = configureStore({
  reducer:  persistedReducer ,
  middleware: [...middlewares],
});

export default store;
export const persistor = persistStore(store);

// types --------------------------------------------
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
